import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Zadanie3 {

    public static void macierze(int tab1[][], int tab2[][], int m, int n, int k) {
        System.out.println("Macierz a:");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.println("[" + i + "]" + "[" + j + "]" + "= " + tab1[i][j]);
            }
        }
        System.out.println("Macierz b:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) {
                System.out.println("[" + i + "]" + "[" + j + "]" + "= " + tab2[i][j]);
            }
        }
        int[][] iloczynmacierzy = new int[m][k];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < k; j++) {
                iloczynmacierzy[i][j]=0;

                for (int l = 0; l < n; l++) {
                    iloczynmacierzy[i][j] += tab1[i][l] * tab2[l][j];
                }

            }

        }
        System.out.println("Macierz c:");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < k; j++) {
                System.out.println("[" + i + "]" + "[" + j + "]" + "= " + iloczynmacierzy[i][j]);

            }
        }
    }


    public static void main(String[] args) {
        System.out.println("Podaj m: ");
        Scanner scan = new Scanner(System.in);
        int m = scan.nextInt();
        System.out.println("Podaj n: ");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        System.out.println("Podaj k: ");
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        int[][] tab1 = new int[m][n];
        int[][] tab2 = new int[n][k];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                tab1[i][j] = ThreadLocalRandom.current().nextInt(-10, 10 + 1);
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) {
                tab2[i][j] = ThreadLocalRandom.current().nextInt(-10, 10 + 1);
            }
        }

        macierze(tab1, tab2, m, n, k);

    }
}