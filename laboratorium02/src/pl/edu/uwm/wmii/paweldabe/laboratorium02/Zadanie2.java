package pl.edu.uwm.wmii.paweldabe.laboratorium02;

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Zadanie2 {

    public static void generuj ( int tab[], int n, int minWartosc, int maxWartosc){
        for (int i = 0; i < n; i++) {
            int x = ThreadLocalRandom.current().nextInt(minWartosc, maxWartosc + 1);
            tab[i] = x;
        }

    }

    public static int ileParzystych(int[] liczby){
        for (int i = 0; i < liczby.length; i++){
        }
        int parzyste=0;
        for (int i=0;i< liczby.length;i++)
        {
            if (liczby[i]%2==0)
            {
                parzyste+=1;
            }
        }
        return parzyste;

    }
    public static int ileNieparzystych(int[] liczby){
        for (int i = 0; i < liczby.length; i++) {
        }
        int nieparzyste=0;
        for (int i=0;i< liczby.length;i++)
        {
            if (liczby[i]%2==0)
            {
                nieparzyste+=1;
            }
        }
        return nieparzyste;

    }

    public static int ileDodatnich(int[] liczby){
        int wyn1=0;
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]>0){
                wyn1+=1;
            }
        }
        return wyn1;
    }
    public static int ileUjemnych(int[] liczby){
        int wyn2=0;
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]<0){
                wyn2+=1;
            }
        }
        return wyn2;
    }
    public static int ileZerowych(int[] liczby){
        int wyn3=0;
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]==0){
                wyn3+=1;
            }
        }
        return wyn3;
    }

    public static int ileMaksymalnych(int[] liczby){
        int max=liczby[0];
        int wyn4=0;
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]>max){
                max=liczby[i];
            }
            if(liczby[i]==max){
                wyn4+=1;
            }

        }
        return wyn4;
    }

    public static int sumaDodatnich(int[] liczby){
        int suma1=0;
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]>0){
                suma1+=liczby[i];
            }
        }
        return suma1;
    }
    public static int sumaUjemnych(int[] liczby){
        int suma2=0;
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]<0){
                suma2+=liczby[i];
            }
        }
        return suma2;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int liczby[])
    {

        int maxIdx = 0, maxLen = 0, currLen = 0, currIdx = 0;

        for (int k = 0; k < liczby.length; k++)
        {
            if (liczby[k] > 0)
            {
                currLen++;

                if (currLen == 1)
                    currIdx = k;
            }
            else
            {
                if (currLen > maxLen)
                {
                    maxLen = currLen;
                }
                currLen = 0;
            }
        }
        return maxLen;
    }

    public static void signum(int[]liczby){
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]>0){
                liczby[i]=1;
            }
            else {
                liczby[i]=-1;
            }
        }
        for(int i=0;i<liczby.length;i++){
            System.out.println(liczby[i]);
        }
    }

    public static void odwrocFragment(int liczby[],int lewy,int prawy){
        int [] tmp = new int [liczby.length];
        for (int i=0;i<liczby.length;i++){
            tmp[i] = liczby[i];
        }
        for (int i = lewy, j = prawy; i <=prawy && j >= lewy; i++, j--) {
            tmp[i] = liczby[j];
        }
        for (int i=0;i<liczby.length;i++){
            liczby[i] = tmp[i];
        }
        for(int i=0;i<liczby.length;i++){
            System.out.println(liczby[i]);
        }

    }


    public static void main(String[] args) {
        System.out.println("Podaj n: ");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println("Podaj Minimalna Wartość: ");
        Scanner scan1= new Scanner(System.in);
        int minWartosc = scan.nextInt();
        System.out.println("Podaj Maksymalną Wartość: ");
        Scanner scan2= new Scanner(System.in);
        int maxWartosc = scan.nextInt();

        int[]liczby=new int[n];
        generuj(liczby,n,minWartosc,maxWartosc);
        signum(liczby);
        odwrocFragment(liczby,2,5);


    }
}