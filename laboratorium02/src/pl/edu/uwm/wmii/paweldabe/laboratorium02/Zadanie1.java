package pl.edu.uwm.wmii.paweldabe.laboratorium02;

import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args) {
        System.out.println("Podaj n: ");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println("Podaj Minimalna Wartość: ");
        Scanner scan1 = new Scanner(System.in);
        int minWartosc = scan.nextInt();
        System.out.println("Podaj Maksymalną Wartość: ");
        Scanner scan2 = new Scanner(System.in);
        int maxWartosc = scan.nextInt();

        int[] liczby = new int[n];

        //a)
        int parz = 0;
        int nieparz = 0;
        for (int i = 0; i < n; i++) {
            if (liczby[i] % 2 == 0) {
                parz += 1;
            } else if (liczby[i] % 2 != 0) {
                nieparz += 1;
            }
        }
        System.out.println("Liczb parzystych jest: " + parz);
        System.out.println("Liczb nieparzystych jest: " + nieparz);

        //b)
        int wyn1=0;
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]>0){
                wyn1+=1;
            }
        }
        int wyn2=0;
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]<0){
                wyn2+=1;
            }
        }
        System.out.println(wyn1);
        System.out.println(wyn2);

        //c)
        int max=liczby[0];
        int wyn3=0;
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]>max){
                max=liczby[i];
            }
            if(liczby[i]==max){
                wyn3+=1;
            }

        }
        System.out.println(wyn3);

        //d)
        int suma1=0;
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]>0){
                suma1+=liczby[i];
            }
        }
        int suma2=0;
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]<0){
                suma2+=liczby[i];
            }
        }
        System.out.println(suma1);
        System.out.println(suma2);

        //e)
        int maxIdx = 0, maxLen = 0, currLen = 0, currIdx = 0;

        for (int k = 0; k < liczby.length; k++)
        {
            if (liczby[k] > 0)
            {
                currLen++;

                if (currLen == 1)
                    currIdx = k;
            }
            else
            {
                if (currLen > maxLen)
                {
                    maxLen = currLen;
                }
                currLen = 0;
            }
        }
        System.out.println(maxLen);
        //f)
        for(int i=0;i< liczby.length;i++){
            if(liczby[i]>0){
                liczby[i]=1;
            }
            else {
                liczby[i]=-1;
            }
        }
        for(int i=0;i<liczby.length;i++){
            System.out.println(liczby[i]);
        }

        //g)
        int prawy=2;int lewy=5;
        int [] tmp = new int [liczby.length];
        for (int i=0;i<liczby.length;i++){
            tmp[i] = liczby[i];
        }
        for (int i = lewy, j = prawy; i <=prawy && j >= lewy; i++, j--) {
            tmp[i] = liczby[j];
        }
        for (int i=0;i<liczby.length;i++){
            liczby[i] = tmp[i];
        }
        for(int i=0;i<liczby.length;i++){
            System.out.println(liczby[i]);
        }
    }

}
