package pl.edu.uwm.wmii.paweldabe.laboratorium06;

public class Main {

    public static void main(String[] args) {
        /////////////////////////////////////////////////////////////    ZADANIE 1
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        saver1.setRocznaStopaProcentowa();
        System.out.println("Po pierwszym miesiacu: ");
        System.out.println(saver1.ObliczMiesieczneOdsetki());
        System.out.println(saver2.ObliczMiesieczneOdsetki());

        saver1.setRocznaStopaProcentowa();
        System.out.println("Po drugim miesiacu: ");
        System.out.println(saver1.ObliczMiesieczneOdsetki());
        System.out.println(saver2.ObliczMiesieczneOdsetki());
        ///////////////////////////////////////////////////////////    ZADANIE 2
        IntegerSet zbior1= new IntegerSet();
        IntegerSet zbior2= new IntegerSet();
        zbior1.insertElement(17);
        zbior1.insertElement(36);
        zbior1.insertElement(20);
        zbior1.insertElement(44);
        zbior2.insertElement(48);
        zbior2.insertElement(12);
        zbior2.insertElement(59);
        zbior2.insertElement(7);
        System.out.println(zbior1);
        System.out.println(zbior2);
        System.out.println(IntegerSet.union(zbior1,zbior2));
        System.out.println(IntegerSet.intersection(zbior1,zbior2));
        zbior1.deleteElement(36);
        System.out.println(zbior1);
        System.out.println(zbior1.equals(zbior2));
        IntegerSet zbior3= new IntegerSet();
        zbior3.insertElement(19);
        zbior3.insertElement(69);
        System.out.println(zbior1.equals(zbior3));
    }
}
