package pl.edu.uwm.wmii.paweldabe.laboratorium06;

public class IntegerSet {
    private final boolean[] zbior;
    public IntegerSet(){
        this.zbior=new boolean[100];
    }
    public static IntegerSet union(IntegerSet zbior1, IntegerSet zbior2){
        IntegerSet nowy_zbior=new IntegerSet();
        for(int i=0;i<100;i++){
            if(zbior1.zbior[i] || zbior2.zbior[i])nowy_zbior.zbior[i]=true;
        }
        return nowy_zbior;
    }
    public static IntegerSet intersection(IntegerSet zbior1, IntegerSet zbior2){
        IntegerSet nowy_zbior= new IntegerSet();
        for(int i=0;i<100;i++){
            if(zbior1.zbior[i] && zbior2.zbior[i])nowy_zbior.zbior[i]=true;
        }
        return nowy_zbior;
    }
    public void insertElement(int i){
        this.zbior[i-1]=true;
    }
    public void deleteElement(int i){
        this.zbior[i-1]=false;
    }
    @Override
    public String toString(){
        StringBuilder liczby= new StringBuilder();
        for(int i=0;i<100;i++){
            if(this.zbior[i]){
                liczby.append(i+1);
                liczby.append(" ");
            }
        }
        return liczby.toString();
    }
    public boolean equals(IntegerSet b){
        return this.toString().equals(b.toString());
    }
}
