package pl.edu.uwm.wmii.paweldabe.laboratorium06;

import java.util.Scanner;

public class RachunekBankowy {
    private static double rocznaStopaProcentowa;
    private double saldo;

    public RachunekBankowy(double saldo) {
        this.saldo = saldo;
    }

    public double ObliczMiesieczneOdsetki(){
        saldo += (saldo * (rocznaStopaProcentowa/100)) /12;
        return saldo;
    }
    public static void setRocznaStopaProcentowa(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj nową roczną stopę procentową: ");
        rocznaStopaProcentowa = scanner.nextDouble();

    }

}


