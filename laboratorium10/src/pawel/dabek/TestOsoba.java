package pawel.dabek;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba {
    public static void main(String[]args){
        ArrayList<Osoba> listaOsob = new ArrayList<>(5);
        listaOsob.add(new Osoba("Dąbek", LocalDate.of(2000,8,17)));
        listaOsob.add(new Osoba("Nowak", LocalDate.of(1999,1,2)));
        listaOsob.add(new Osoba("Nowak", LocalDate.of(1968,11,25)));
        listaOsob.add(new Osoba("Lipka", LocalDate.of(1998,3,13)));
        listaOsob.add(new Osoba("Grucha", LocalDate.of(1998,3,13)));
        System.out.println(listaOsob);
        Collections.sort(listaOsob);
        System.out.println(listaOsob);
    }
}
