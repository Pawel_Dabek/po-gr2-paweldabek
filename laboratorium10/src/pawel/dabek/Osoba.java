package pawel.dabek;

import java.time.LocalDate;

public class Osoba implements Cloneable,Comparable<Osoba>{
    private String nazwisko;
    private LocalDate dataUrodzenia;
    public Osoba(String nazwisko,LocalDate dataUrodzenia){
        this.dataUrodzenia=dataUrodzenia;
        this.nazwisko=nazwisko;
    }

    public String getNazwisko(){
        return this.nazwisko;
    }
    public LocalDate getDataUrodzenia(){
        return this.dataUrodzenia;
    }

    @Override
    public boolean equals(Object obj) {
        Osoba os = (Osoba) obj;
        return (os.nazwisko.equals(this.nazwisko) && os.dataUrodzenia.equals(this.dataUrodzenia));
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"["+this.nazwisko+","+this.dataUrodzenia.toString()+"]";
    }

    @Override
    public int compareTo(Osoba o) {
        int compare_naz=this.nazwisko.compareTo(o.nazwisko);
        if(compare_naz==0){
            return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        }
        return compare_naz;
    }
}
