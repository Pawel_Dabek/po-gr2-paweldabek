package pawel.dabek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[]args){
        ArrayList<Osoba> listaStudentow = new ArrayList<>(5);
        listaStudentow.add(new Osoba("Dąbek", LocalDate.of(2000,8,17)));
        listaStudentow.add(new Osoba("Kołacz", LocalDate.of(1999,1,2)));
        listaStudentow.add(new Osoba("Kołacz", LocalDate.of(2001,11,25)));
        listaStudentow.add(new Osoba("Bączkowski", LocalDate.of(1998,3,13)));
        listaStudentow.add(new Osoba("Judas", LocalDate.of(1998,3,13)));
        System.out.println(listaStudentow);
        Collections.sort(listaStudentow);
        System.out.println(listaStudentow);
    }
}
