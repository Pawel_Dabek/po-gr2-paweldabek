package pl.edu.uwm.wmii.paweldabe.laboratorium04;

import java.math.BigInteger;

public class zadanie4 {
    public static BigInteger szachownica (int n){
        BigInteger wynik = new BigInteger(String.valueOf(1));
        BigInteger x = new BigInteger(String.valueOf(2));
        BigInteger [] tab = new BigInteger[n*n];
        tab[0] = wynik;
        BigInteger suma = new BigInteger(String.valueOf(0));
        for (int i=1;i<n*n;i++){

            tab[i] = tab[i-1].multiply(x);
        }
        for (int i=0;i<n*n;i++){
            suma = suma.add(tab[i]);
        }
        return suma;
    }
    public static void main(String[] args){
        System.out.println(szachownica(2));
    }
}