package pl.edu.uwm.wmii.paweldabe.laboratorium04;

public class zadanie1b {
    public static int countSubStr(String str, String subStr) {
        int lastIndex = 0;
        int count = 0;

        while (lastIndex != -1) {

            lastIndex = str.indexOf(subStr, lastIndex);

            if (lastIndex != -1) {
                count++;
                lastIndex += subStr.length();
            }
        }
            return count;
        }
        public static void main (String [] args){

            System.out.println(countSubStr("pabpabpaabpablo", "pab"));

        }
}


