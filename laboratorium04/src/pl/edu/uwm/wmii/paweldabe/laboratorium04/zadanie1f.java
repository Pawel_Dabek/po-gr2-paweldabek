package pl.edu.uwm.wmii.paweldabe.laboratorium04;

import java.util.Scanner;

public class zadanie1f{
    public static String change(String str){
        StringBuffer slowo = new StringBuffer();
        slowo.append(str);
        for (int i=0; i<slowo.length();i++){
            if (slowo.charAt(i) >=65 && slowo.charAt(i) <=90 ){ ////wartości brane z tablicy ASCI
                char litera = slowo.charAt(i);
                slowo.setCharAt(i, Character.toLowerCase(litera));
            }
            else{
                char litera = slowo.charAt(i);
                slowo.setCharAt(i, Character.toUpperCase(litera));
            }
        }
        return slowo.toString();
    }
    public static void main(String[] args){
        System.out.println("Podaj slowo: ");
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        System.out.println(change(s));
    }
}


