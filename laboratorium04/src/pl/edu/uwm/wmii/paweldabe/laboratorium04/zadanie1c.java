package pl.edu.uwm.wmii.paweldabe.laboratorium04;

public class zadanie1c {
    public static String middle(String str) {
        int position;
        int length;
        if (str.length() % 2 == 0)
        {
            position = str.length() / 2 - 1;
            length = 2;
        }
        else
        {
            position = str.length() / 2;
            length = 1;
        }
        return str.substring(position, position + length);
    }


    public static void main (String [] args){
        System.out.println(middle("Pabblo"));


    }
}


