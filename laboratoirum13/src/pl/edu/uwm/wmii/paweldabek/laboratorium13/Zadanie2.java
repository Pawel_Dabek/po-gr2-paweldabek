package pl.edu.uwm.wmii.paweldabek.laboratorium13;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zadanie2 {
    public static void dziennik(){
        HashMap<String, String> oceny = new HashMap<String, String>();
        while(true){
            System.out.println("Podaj Komende(dodaj, wyswietl, usun, edytuj lub zakoncz):");
            Scanner scan = new Scanner(System.in);
            String komenda = scan.nextLine();

            if(komenda.equals("dodaj")){
                System.out.println("Podaj ucznia i ocene");
                Scanner scan2 = new Scanner(System.in);
                System.out.println("Imie:");
                String imie = scan2.nextLine();

                Scanner scan3 = new Scanner(System.in);
                System.out.println("Ocena:");
                String ocena = scan3.nextLine();
                System.out.println("Uczen: "+imie+" ocena: "+ocena);

                oceny.put(imie, ocena);
                System.out.println("Pomyślnie dodano ucznia");


            }
            if(komenda.equals("wyswietl")){
                TreeMap<String, String> sorted = new TreeMap<>();
                sorted.putAll(oceny);
                for (Map.Entry<String, String> entry : sorted.entrySet())
                    System.out.println(entry.getKey() +" : "+ entry.getValue());
            }
            if(komenda.equals("usun")){
                System.out.println("Podaj imie ucznia");
                Scanner scan3 = new Scanner(System.in);
                System.out.println("Imie:");
                String imie = scan3.nextLine();

                oceny.remove(imie);
                System.out.println("Pomyślnie usunięto ucznia");
            }
            if(komenda.equals("edytuj")){
                System.out.println("Podaj imie ucznia");
                Scanner scan4 = new Scanner(System.in);
                System.out.println("Imie:");
                String imie = scan4.nextLine();

                Scanner scan3 = new Scanner(System.in);
                System.out.println("Nowa ocena:");
                String ocena = scan3.nextLine();

                oceny.replace(imie, ocena);
            }
            if(komenda.equals("zakoncz")){
                System.out.println("Zamykam program.");
                break;
            }
        }
    }
    public static void main(String[] args) {
        dziennik();
    }
}