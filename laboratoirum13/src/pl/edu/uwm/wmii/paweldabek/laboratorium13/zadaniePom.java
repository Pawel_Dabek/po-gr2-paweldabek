package pl.edu.uwm.wmii.paweldabek.laboratorium13;

public class zadaniePom implements Comparable<zadaniePom>{
    private String opis;
    private int priorytet;

    public zadaniePom(String opis, int priorytet) {
        this.opis = opis;
        this.priorytet = priorytet;
    }

    @Override
    public String toString() {
        return "zadaniePom{" +
                "opis='" + opis + '\'' +
                ", priorytet=" + priorytet +
                '}';
    }

    @Override
    public int compareTo(zadaniePom o) {
        if (priorytet < o.priorytet)
            return -1;
        else if (priorytet > o.priorytet)
            return 1;
        return 0;
    }
}