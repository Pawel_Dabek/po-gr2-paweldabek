package pl.edu.uwm.wmii.paweldabe.laboratorium01;

import java.util.Scanner;

public class Zadanie2_3 {
    public static void main(String[] args) {
        System.out.println("Podaj n:");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        float[] tab = new float[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj " + i + " liczbę");
            Scanner scan = new Scanner(System.in);
            tab[i] = scan.nextFloat();
        }
        int dodatnie = 0;
        int ujemne = 0;
        int zera = 0;
        for (int i = 0; i < n; i++) {
            if (tab[i] > 0) {
                dodatnie += 1;
            } else if (tab[i] < 0) {
                ujemne += 1;
            } else {
                zera += 1;
            }
        }
        System.out.println("Liczb dodatnich jest: "+dodatnie+" Ujemnych jest: "+ujemne+" Zer jest:"+zera);
    }
}
