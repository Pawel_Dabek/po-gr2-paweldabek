package pl.edu.uwm.wmii.paweldabe.laboratorium01;

import java.util.Scanner;

public class Zadanie2_2 {
    public static void main(String[] args){
        System.out.println("Podaj n:");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        float [] tab = new float[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj " + i + " liczbę");
            Scanner scan = new Scanner(System.in);
            tab[i] = scan.nextFloat();
        }
        float sum=0;
        for(int i=0;i<n;i++)
        {
            if (tab[i]>0)
            {
                sum+=tab[i];
            }
        }
        System.out.println(sum*2);


    }
}