package pl.edu.uwm.wmii.paweldabe.laboratorium01;

import java.util.Scanner;

public class Zadanie2_5 {
    public static void main(String[] args) {
        System.out.println("Podaj n:");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        float[] tab = new float[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj " + i + " liczbę");
            Scanner scan = new Scanner(System.in);
            tab[i] = scan.nextFloat();
        }
        int pary=0;
        for (int i=1;i<n;i++)
        {
            if (tab[i-1]>0 && tab[i]>0)
            {
                pary+=1;
            }
        }

        System.out.println("Ilosc takich par: "+pary);

    }
}
