import java.util.Scanner;

public class Zadanie2_1 {
    private static int silnia(int n) {
        int wynik1 = 1;
        for (int i = 1; i <= n; i++) {
            wynik1 *= i;
        }
        return wynik1;
    }
    private static int a (int n, int[] liczby) {
        int[] tab = new int[n];
        for (int i = 0; i < n; i++) {
            tab[i] = liczby[i];
        }
        int a = 0;
        for (int i = 0; i < n; i++) {
            if (tab[i] % 2 != 0) {
                a += 1;
            }
        }
        return a;
    }
    private static int b(int n,int[] liczby){
        int [] tab = new int[n];
        for (int i = 0; i < n; i++) {
            tab[i] = liczby[i];
        }
        int b=0;
        for (int i=0;i<n;i++)
        {
            if (tab[i]%3==0 && tab[i]%5!=0)
            {
                b+=1;
            }
        }
        return b;

    }
    private static int c(int n,int[] liczby){
        int [] tab = new int[n];
        for (int i = 0; i < n; i++) {
            tab[i] = liczby[i];
        }
        int c=0;
        for (int i=0;i<n;i++)
        {
            float pol = tab[i]/2;
            if ((tab[i]/pol)==pol && tab[i]%2==0)
            {
                c+=1;
            }
        }
        return c;
    }
    private static int d (int n,int[] liczby) {
        int[] tab = new int[n];
        for (int i = 0; i < n; i++) {
            tab[i] = liczby[i];
        }
        int d = 0;
        for (int k = 1; k < n - 1; k++) {
            if (tab[k] < ((tab[k - 1] + tab[k + 1]) / 2)) {
                d += 1;
            }
        }
        return d;
    }
    private static int e (int n,int[] liczby){
        int[] tab = new int[n];
        for (int i = 0; i < n; i++) {
            tab[i] = liczby[i];
        }
        int e=0;
        for (int k=1;k<n;k++)
        {
            if (tab[k]> Math.pow(2,k) && tab[k] < silnia(k))
            {
                e+=1;
            }
        }
        return e;

    }
    private static int f (int n,int liczby[]){
        int[] tab = new int[n];
        for (int i = 0; i < n; i++) {
            tab[i] = liczby[i];
        }
        int f=0;
        for (int i=1;i<n;i++)
        {
            if (i%2!=0 && tab[i]%2==0)
            {
                f+=1;
            }
        }
        return f;

    }
    private static int g (int n,int[] liczby){
        int[] tab = new int[n];
        for (int i = 0; i < n; i++) {
            tab[i] = liczby[i];
        }
        int g = 0;
        for (int i=0;i<n;i++)
        {
            if (tab[i]>=0 && tab[i]%2!=0)
            {
                g+=1;
            }
        }
        return g;

    }
    private static int h (int n,int[] liczby){
        int[] tab = new int[n];
        for (int i = 0; i < n; i++) {
            tab[i] = liczby[i];
        }
        int h=0;
        for(int i=0; i<n;i++)
        {
            if (tab[i] < Math.pow(i,2))
            {
                h+=1;
            }
        }
        return h;

    }
    public static void main(String[] args) {
        System.out.println("Ile chcesz podać liczb:");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] liczby = new int[n];
        int liczba = 0;
        for(int i=0; i<n; i++){
            System.out.println("Podaj liczbe:");
            liczba = scan.nextInt();
            liczby[i]=liczba;
        }
        System.out.println(a(n,liczby));
        System.out.println(b(n,liczby));
        System.out.println(c(n,liczby));
        System.out.println(d(n,liczby));
        System.out.println(e(n,liczby));
        System.out.println(f(n,liczby));
        System.out.println(g(n,liczby));
        System.out.println(h(n,liczby));
    }
}
