import java.util.Scanner;

public class Zadanie1 {

    private static float a(int n, float[] tab) {
        float wyn1 = 0;
        for (int i = 0; i < n; i++) {
            wyn1 += tab[i];
        }
        return wyn1;
    }

    private static float b(int n, float[] tab) {
        float wyn2 = 1;
        for (int i = 0; i < n; i++) {
            wyn2 *= tab[i];
        }
        return wyn2;

    }

    private static float c(int n, float[] tab) {
        float wyn3 = 0;
        for (int i = 0; i < n; i++) {
            wyn3 += Math.abs(tab[i]);
        }
        return wyn3;
    }

    private static float d(int n, float[] tab) {
        float wyn4 = 0;
        for (int i = 0; i < n; i++) {
            wyn4 += Math.sqrt(Math.abs(tab[i]));
        }
        return wyn4;
    }

    private static float e(int n, float[] tab) {
        float wyn5 = 1;
        for (int i = 0; i < n; i++) {
            wyn5 *= Math.abs(tab[i]);
        }
        return wyn5;

    }

    private static float f(int n, float[] tab) {
        float wyn6 = 0;
        for (int i = 0; i < n; i++) {
            wyn6 += wyn6 * wyn6;
        }
        return wyn6;
    }

    private static void g (int n, float [] tab) {
        float dod = 0;
        float mnoz = 1;
        for (int i = 0; i < n; i++) {
            dod += tab[i];
            mnoz *= tab[i];
        }
        System.out.println("Wynik dodawania "+dod);
        System.out.println("Wynik mnozenia "+mnoz);

    }
    private static float h(int n, float[] tab) {
        float wyn7 = 0;
        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                tab[i] = tab[i] * (-1);
            }
            wyn7 = wyn7 + tab[i];

        }
        return wyn7;
    }

    private static double i(int n, float[] tab) {
        int mian = 1;
        double wyn8 = 0;
        for (int i = 0; i < n; i++) {
            mian = mian * i;
            wyn8 = wyn8 + (Math.pow(-1, i) * tab[i]) / mian;
        }
        return wyn8;

    }

    public static void main(String[] args) {
        System.out.println("Ile chcesz podać liczb:");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        float[] tab = new float[n];
        float liczba = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe:");
            liczba = scan.nextFloat();
            tab[i] = liczba;
        }

        System.out.println("a) = " + a(n, tab));
        System.out.println("b) = " + b(n, tab));
        System.out.println("c) = " + c(n, tab));
        System.out.println("d) = " + d(n, tab));
        System.out.println("e) = " + e(n, tab));
        System.out.println("f) = " + f(n, tab));
        System.out.println("g) = ");
        g(n,tab);
        System.out.println("h) = " + h(n, tab));
        System.out.println("i) = " + i(n, tab));
    }
}