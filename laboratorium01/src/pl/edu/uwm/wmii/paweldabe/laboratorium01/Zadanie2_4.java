package pl.edu.uwm.wmii.paweldabe.laboratorium01;

import java.util.Scanner;

public class Zadanie2_4 {
    public static void main(String[] args) {
        System.out.println("Podaj n:");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        float[] tab = new float[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj " + i + " liczbę");
            Scanner scan = new Scanner(System.in);
            tab[i] = scan.nextFloat();
        }
        float max=tab[0];
        float min=tab[0];
        for (int i=0;i<n;i++)
        {
            if (tab[i]>max)
            {
                max = tab[i];
            }
            if (tab[i]<min)
            {
                min = tab[i];
            }
        }
        System.out.println("Najwieksza liczba to: "+max+" Najmniejsza to: "+min);
    }
}