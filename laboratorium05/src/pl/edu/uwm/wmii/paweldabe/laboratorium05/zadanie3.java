package pl.edu.uwm.wmii.paweldabe.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class zadanie3 {

    public static ArrayList<Integer>mergeSorted(ArrayList<Integer>a, ArrayList<Integer>b){

        for (int index2 = 0; index2 < b.size(); index2++) {
            for (int index1 = 0; ; index1++) {
                if (index1 == a.size() || a.get(index1) > b.get(index2)) {
                    a.add(index1, b.get(index2));
                    break;
                }
            }
        }
        return a;
    }

    public static void main(String[] args){
        ArrayList<Integer> listOne = new ArrayList<>(Arrays.asList(1,6,7,20));

        ArrayList<Integer> listTwo = new ArrayList<>(Arrays.asList(17,3,5,8,3));

        System.out.println(mergeSorted(listOne,listTwo));
    }
}
