package pl.edu.uwm.wmii.paweldabe.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class zadanie2 {

    public static ArrayList<Integer> merge(ArrayList<Integer>a, ArrayList<Integer>b){
        int c1 = 0, c2 = 0;
        ArrayList<Integer> res = new ArrayList<Integer>();

        while(c1 < a.size() || c2 < b.size()) {
            if(c1 < a.size())
                res.add((Integer) a.get(c1++));
            if(c2 < b.size())
                res.add((Integer) b.get(c2++));
        }
        return res;
    }

    public static void main(String [] args){
        ArrayList<Integer> listOne = new ArrayList<>(Arrays.asList(1,6,7,20));

        ArrayList<Integer> listTwo = new ArrayList<>(Arrays.asList(20,17,3,5,8));

        System.out.println(merge(listOne,listTwo));
    }
}
