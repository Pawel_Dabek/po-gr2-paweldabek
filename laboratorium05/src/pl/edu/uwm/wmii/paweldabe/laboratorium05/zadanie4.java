package pl.edu.uwm.wmii.paweldabe.laboratorium05;

import java.io.*;
import java.util.*;
class zadanie4 {


    public ArrayList<Integer> reversed(ArrayList<Integer> alist)
    {

        ArrayList<Integer> revArrayList = new ArrayList<Integer>();
        for (int i = alist.size() - 1; i >= 0; i--) {
            revArrayList.add(alist.get(i));
        }
        return revArrayList;
    }

    public void printElements(ArrayList<Integer> alist)
    {
        for (int i = 0; i < alist.size(); i++) {
            System.out.print(alist.get(i) + " ");
        }
    }


    public static void main(String[] args)
    {
        zadanie4 obj = new zadanie4();

        ArrayList<Integer> arrayli = new ArrayList<Integer>();

        arrayli.add(new Integer(1));
        arrayli.add(new Integer(20));
        arrayli.add(new Integer(7));
        arrayli.add(new Integer(21));
        arrayli.add(new Integer(3));
        arrayli.add(new Integer(44));
        arrayli.add(new Integer(15));
        arrayli.add(new Integer(9));
        System.out.print("Przed odwróceniem: ");
        obj.printElements(arrayli);
        arrayli = obj.reversed(arrayli);
        System.out.print("\nPo odwróceniu: ");
        obj.printElements(arrayli);
    }
}