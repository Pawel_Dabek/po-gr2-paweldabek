package pl.edu.uwm.wmii.paweldabe.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class zadanie1 {
   public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        a.addAll(b);

        return a;
    }

    public static void main(String[] args){
        ArrayList<Integer> listOne = new ArrayList<>(Arrays.asList(1,6,7,20,9));

        ArrayList<Integer> listTwo = new ArrayList<>(Arrays.asList(20,17,3,5,8));

        System.out.println(append(listOne,listTwo));
    }
}
