package pl.edu.uwm.wmii.paweldabe.laboratorium05;

import java.util.ArrayList;

public class zadanie5 {
    public static void reverse(ArrayList<Integer>a){

        ArrayList<Integer> revArrayList = new ArrayList<Integer>();
        for (int i = a.size() - 1; i >= 0; i--) {
            revArrayList.add(a.get(i));
        }
        System.out.println(revArrayList);

    }

    public static void main(String[] args){

        ArrayList<Integer> liczby = new ArrayList<Integer>();
        liczby.add(1);
        liczby.add(2);
        liczby.add(3);
        liczby.add(4);

        reverse(liczby);

    }
}
