package pawel.dabek;

import java.time.LocalDate;

public class Pracownik extends Osoba{
    public Pracownik(String nazwisko, String[] imiona, LocalDate DU, boolean plec, double pobory, LocalDate DZ)
    {
        super(nazwisko,imiona,DU,plec);
        this.pobory = pobory;
        this.DataZatrudnienia=DZ;
    }

    public double getPobory()
    {
        return pobory;
    }

    public LocalDate getDataZatrudnienia() {
        return DataZatrudnienia;
    }

    public String getOpis()
    {
        return String.format("Pracownik zatrudniony %s z pensją %.2f zł", this.DataZatrudnienia.toString(),this.pobory);
    }

    private double pobory;
    private LocalDate DataZatrudnienia;
}