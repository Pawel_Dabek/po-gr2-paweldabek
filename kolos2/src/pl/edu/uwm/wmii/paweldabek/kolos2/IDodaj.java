package pl.edu.uwm.wmii.paweldabek.kolos2;

public interface IDodaj {
    public void DodajPolaczenie(String numer, int czasP);
    public void DodajSms(String numer);
    public void DodajInternet(int iloscMB);
}