package pl.edu.uwm.wmii.paweldabek.kolos2;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Telefon implements IBiling,IDodaj {

    public Telefon() {

    }

    @Override
    public void ZapiszBiling() throws FileNotFoundException {
        PrintWriter zapis = new PrintWriter("biling.txt");
        zapis.print(biling);
        zapis.close();
    }

    @Override
    public void DodajPolaczenie(String numer, int czasP) {
        biling.add(new Polaczenia(LocalDateTime.now(), numer, czasP));
    }

    @Override
    public void DodajSms(String numer) {
        biling.add(new Sms(LocalDateTime.now(), numer));
    }

    @Override
    public void DodajInternet(int iloscMB) {
        biling.add(new Internet(LocalDateTime.now(), iloscMB));
    }

    private List<Usluga> biling;
    {
        biling = new ArrayList<>();
    }

    @Override
    public String toString()
    {
        return biling.toString();
    }
}
