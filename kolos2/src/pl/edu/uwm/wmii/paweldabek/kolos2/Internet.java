package pl.edu.uwm.wmii.paweldabek.kolos2;

import java.time.LocalDateTime;

public class Internet extends Usluga
{
    public Internet(LocalDateTime czas, int iloscMB)
    {
        super(czas);
        this.iloscMB = iloscMB;
        obliczCene();
    }

    @Override
    public void obliczCene()
    {
        this.cena = Math.round(this.iloscMB/755 * 100.0) / 100.0;
    }

    private int iloscMB;

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+" Internet, data i godzina: "+this.czas+" IloscMB: "+this.iloscMB+" Laczny koszt : "+this.cena+"\n";
    }
}