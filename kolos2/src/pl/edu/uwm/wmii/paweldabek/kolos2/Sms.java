package pl.edu.uwm.wmii.paweldabek.kolos2;

import java.time.LocalDateTime;

public class Sms extends Usluga
{
    public Sms(LocalDateTime czas, String numer)
    {
        super(czas);
        this.numer = numer;
        obliczCene();
    }

    @Override
    public void obliczCene()
    {
        this.cena = 0.13;
    }
    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+" Sms: "+this.numer+" Data i godzina smsa: "+this.czas+" Laczny koszt : "+this.cena+"\n";
    }

    private String numer;
}