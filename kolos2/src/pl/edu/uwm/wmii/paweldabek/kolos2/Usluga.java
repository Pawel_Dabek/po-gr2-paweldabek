package pl.edu.uwm.wmii.paweldabek.kolos2;

import java.time.LocalDate;
import java.time.LocalDateTime;

public abstract class Usluga
{
    public Usluga(LocalDateTime czas)
    {
        this.czas = czas;
    }

    public abstract void obliczCene();

    protected LocalDateTime czas;
    protected double cena;
}
