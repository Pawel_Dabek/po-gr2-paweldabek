package pl.edu.uwm.wmii.paweldabek.kolos2;

import java.time.LocalDateTime;

class Polaczenia extends Usluga
{
    public Polaczenia(LocalDateTime czas, String numer, int czasP)
    {
        super(czas);
        this.numer = numer;
        this.czasP = czasP;
        obliczCene();
    }

    @Override
    public void obliczCene()
    {
        this.cena = czasP * 0.27;
    }

    private String numer;
    private int czasP;

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+" Polaczenie: "+this.numer+" Data i godzina rozmowy: "+this.czas+" Dlugosc trwania :"+this.czasP+" Laczny koszt : "+this.cena+"\n";
    }
}

