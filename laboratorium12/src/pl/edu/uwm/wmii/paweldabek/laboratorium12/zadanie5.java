package pl.edu.uwm.wmii.paweldabek.laboratorium12;
import java.util.Stack;
public class zadanie5 {
    public static String odwracanie_zdan(String zdanie){
        String[] slowa= zdanie.split(" ");
        Stack<String> odwracanie_zdan= new Stack<>();
        StringBuilder odwrocone= new StringBuilder();
        for(String slowo: slowa){
            odwracanie_zdan.push(slowo);
            if(slowo.endsWith(".")){
                while(!odwracanie_zdan.empty()){
                    StringBuilder slowo_odwrocone = new StringBuilder();
                    slowo_odwrocone.append(odwracanie_zdan.pop());
                    if(odwracanie_zdan.empty()){
                        slowo_odwrocone.setCharAt(0,Character.toLowerCase(slowo_odwrocone.charAt(0)));
                        odwrocone.append(slowo_odwrocone);
                        odwrocone.append(". ");
                    }
                    else if(slowo_odwrocone.toString().equals(slowo)){
                        slowo_odwrocone.setCharAt(0,Character.toUpperCase(slowo_odwrocone.charAt(0)));
                        odwrocone.append(slowo_odwrocone, 0, slowo_odwrocone.length()-1);
                        odwrocone.append(" ");
                    }
                    else{
                        odwrocone.append(slowo_odwrocone);
                        odwrocone.append(" ");
                    }
                }
            }
        }
        return odwrocone.toString();
    }
}

