package pl.edu.uwm.wmii.paweldabek.laboratorium12;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        //ZADANIE 1

        LinkedList<String> pracownicy = new LinkedList<String>();
        pracownicy.add("Wojtek Kozak");
        pracownicy.add("Marcin Bułka");
        pracownicy.add("Franek Kimono");
        pracownicy.add("Krzysiek Protko");

        System.out.print("Pracownicy przed redukcja:  " + pracownicy);

        zadanie1.redukuj(pracownicy,2);

        System.out.println("\nPracownicy po redukcji: " + pracownicy);

        //ZADANIE 3

        LinkedList<String> cars = new LinkedList<String>();
        cars.add("Volvo");
        cars.add("BMW");
        cars.add("Ford");
        cars.add("Mazda");

        System.out.print("Elementy przed odwróceniem: " + cars);

        zadanie3.odwroc(cars);;

        System.out.print("\nElementy po odwróceniu: " + cars);

        //ZADANIE 5

        System.out.println(zadanie5.odwracanie_zdan("Ala ma kota. Jej kot lubi myszy."));

        //ZADANIE 6

        zadanie6.cyfry(2015);

        //ZADANIE 7

        zadanie7.Erastotenes(100);

        //ZADANIE 8

        LinkedList<Integer> liczby = new LinkedList<>();
        liczby.add(1);
        liczby.add(2);
        liczby.add(3);
        liczby.add(4);
        liczby.add(5);
        liczby.add(6);
        liczby.add(7);
        liczby.add(8);
        liczby.add(9);

        HashSet<LocalDate> daty = new HashSet<>();
        daty.add(LocalDate.of(2020,1,8));
        daty.add(LocalDate.of(2020,2,9));
        daty.add(LocalDate.of(2020,3,10));
        daty.add(LocalDate.of(2020,4,11));
        daty.add(LocalDate.of(2020,5,12));
        daty.add(LocalDate.of(2020,6,13));
        daty.add(LocalDate.of(2020,7,14));
        zadanie8.print(liczby);
        zadanie8.print(pracownicy);
        zadanie8.print(daty);
    }
}
