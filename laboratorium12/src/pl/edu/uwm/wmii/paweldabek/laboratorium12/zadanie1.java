package pl.edu.uwm.wmii.paweldabek.laboratorium12;

import java.util.LinkedList;

public class zadanie1 {
    public static <T> void redukuj(LinkedList<T> pracownicy, int n){
        for(int i=n-1; i<pracownicy.size();i+=n-1){
            pracownicy.remove(i);
        }
    }
}
