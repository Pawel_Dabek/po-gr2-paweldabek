package pl.edu.uwm.wmii.paweldabek.laboratorium12;

import java.util.Stack;

public class zadanie6 {

    public static void cyfry(int liczba){
        Stack<Integer> cyferki = new Stack<>();
        while(liczba!=0){
            cyferki.push(liczba%10);
            liczba/=10;
        }
        while (!cyferki.empty()){
            System.out.print(cyferki.pop()+" ");
        }
        System.out.println();
    }
}
