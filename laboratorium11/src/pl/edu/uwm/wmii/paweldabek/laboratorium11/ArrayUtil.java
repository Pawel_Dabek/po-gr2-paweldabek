package pl.edu.uwm.wmii.paweldabek.laboratorium11;


public class ArrayUtil<T> {
    T[] tab;

    public ArrayUtil(T[] tab) {
        this.tab = tab;
    }

    static <T extends Comparable> boolean isSorted(T[] a) {
        if (a == null || a.length == 0) {
            return false;
        }
        for (int i = 1; i < a.length; i++) {
            if (a[i - 1].compareTo(a[i]) > 0) {
                return false;
            }
        }
        return true;
    }

    static <T extends Comparable> int binSearch(T[] a, T s) {
        int ind = -1;
        for (int i = 0; i < a.length; i++) {
            if (a[i].equals(s)) {
                ind = i;
            }
        }
        return ind;

    }

    static <T extends Comparable> void SelectionSort(T[] a) {
        T tmp;
        for (int i = 0; i < a.length; i++) {
            for (int j = i; j < a.length; j++) {
                if (a[i].compareTo(a[j]) >= 0) {
                    tmp = a[j];
                    a[j] = a[i];
                    a[i] = tmp;
                }
            }
        }
    }
}
