package pl.edu.uwm.wmii.paweldabek.laboratorium11;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {

        //ZAD3
        Integer[] intniesort = new Integer[]{1, 4, 6, 2, 10};
        Integer[] intsort = new Integer[]{1, 3, 6, 5, 9, 8};
        System.out.println(ArrayUtil.isSorted(intniesort));
        System.out.println(ArrayUtil.isSorted(intsort));
        LocalDate[] datesort = new LocalDate[]{LocalDate.parse("2020-08-17"), LocalDate.parse("2020-01-21"), LocalDate.parse("2021-04-07")};
        LocalDate[] dateniesort = new LocalDate[]{LocalDate.parse("2020-11-17"), LocalDate.parse("2020-12-25"), LocalDate.parse("2019-01-01")};
        System.out.println(ArrayUtil.isSorted(datesort));
        System.out.println(ArrayUtil.isSorted(dateniesort));

        //ZAD4
        Integer[] tab = new Integer[]{1, 3, 6, 2, 7};
        System.out.println(ArrayUtil.binSearch(tab, 7));
        LocalDate[] tab2 = new LocalDate[]{LocalDate.parse("2020-08-17"), LocalDate.parse("2020-10-10"), LocalDate.parse("2021-01-01")};
        System.out.println(ArrayUtil.binSearch(tab2, LocalDate.parse("2021-01-01")));

        //ZAD5
        Integer[] tabs = new Integer[]{2, 5, 6, 7, 3, 2, 1};
        ArrayUtil.SelectionSort(tabs);
        for (int i : tabs) {
            System.out.println(i);
        }
        LocalDate[] tab5 = new LocalDate[]{LocalDate.parse("2020-12-17"), LocalDate.parse("2020-10-10"), LocalDate.parse("2021-01-01")};
        ArrayUtil.SelectionSort(tab5);
        for (LocalDate j : tab5) {
            System.out.println(j);
        }
    }
}
