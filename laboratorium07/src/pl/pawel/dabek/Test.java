package pl.pawel.dabek;

public class Test {
    public static void main(String[] args) {
        Adres ad1= new Adres("Myszki Miki","6","82-300","Olsztyn");
        Adres ad2= new Adres("Myszki Miki","6", "2","82-301","Olsztyn");
        Adres ad3= new Adres("Myszki Miki","6", "2","82-299","Olsztyn");
        ad1.pokaz();
        ad2.pokaz();
        System.out.println(ad1.przed(ad2));
        System.out.println(ad1.przed(ad3));

        Osoba Marcin= new Osoba("Kierwiński",2000);
        Student Olaf = new Student("Mućko",1995,"Filologia Angielska");
        Nauczyciel Artur = new Nauczyciel("Morski",1973,4000);
        System.out.println(Marcin);
        System.out.println(Olaf);
        System.out.println(Artur);
        System.out.println(Olaf.getKierunek());
        System.out.println(Olaf.getNazwisko());
        System.out.println(Olaf.getRokUrodzenia());
        System.out.println(Artur.getPensja());

        BetterRectangle prostokat= new BetterRectangle(1,1,4,3);
        System.out.println(prostokat.getArea());
        System.out.println(prostokat.getPerimeter());
    }
}